%define head 0

%macro colon 2
    %ifid %2
        %ifstr %1
            %2:
                dq head
                db %1, 0 ;making null-terminated string
            %define head %2
        %else 
            %error "Ivalid key"
        %endif
    %else
        %error "Invalid label"
    %endif
%endmacro