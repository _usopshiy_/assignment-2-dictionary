import subprocess;

input_strings = ["", "AHAHAH_FOR_YOU_WORD_RUM_AND_WORD_DEATH_MEAN_THE_SAME_AHAHAAH", "third", "lul", "first", "ahahahahaahhahahahaahahathisahaahahahahahahhahahahastringahahahahahahahahahahahahahahahahahahahahisahahahahahahahaahahahmoreahahahahaahahahahathenahahahahah255ahahahahahahahahahahahahahaahsymbolsahahahahahahahahahahahahahahahahahinahahahahahahahlengthahahahahahaha"]
outs = ["", "", "third word explanation", "", "first word explanation", ""]
errors = ["No such key found!", "No such key found!", "", "No such key found!", "", "Key is too long!"]
tests_results = ""
print("Running tests!")
print("="*55)
for i in range(len(input_strings)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=input_strings[i].encode())
    str_out = stdout.decode().strip()
    str_err = stderr.decode().strip()
    if str_out == outs[i] and str_err == errors[i]:
        tests_results += "."
        print(f"Test {i+1} passed")
        print("-"*55)
    else:
        tests_results += "F"
        print(f"Test {i+1} failed! Got: ( {str_out} | {str_err} ) Expected: ( {outs[i]} | {errors[i]} )")
        print("-"*55)
print(tests_results)
print("="*55)
