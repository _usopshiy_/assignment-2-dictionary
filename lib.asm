global exit
global string_length
global print_string
global print_error ;new one
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 ;Be aware - I haven't change it since initial mr to git
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; rax = 0
    .loop:
        cmp byte [rdi + rax], 0 ; checking for terminator
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ;save caller-saved register
    call string_length ;no xor for rax since it's included in string_length
    pop rsi ; restore rdi directly to rsi 
    mov rdx, rax ; rax - string_length
    mov rax, 1 ; output call
    mov rdi, 1 ; stdout
    syscall
    ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2 ; stderr
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi 
    mov rdx, 1
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA 
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    xor rcx, rcx
    .read_loop:
        xor rdx, rdx 
        div r8
        add rdx, '0'
        push rdx
        inc rcx
        test rax, rax
        jnz .read_loop
    .print_loop:
        pop rdi
        push rcx
        call print_char
        pop rcx
        dec rcx
        test rcx, rcx 
        jnz .print_loop
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    js .neg
    .pos: ; print positive number
        call print_uint
        ret
    .neg: ; print negative number
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi 
        neg rdi
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .compare_loop:
        mov r8b, byte [rdi + rcx] ; mov symbol of 1st string
        cmp r8b, byte [rsi + rcx] ; compare it with symbol of 2nd string
        jne .neq
        test r8, r8 ; check for terminator
        jz .eq
        inc rcx
        jmp .compare_loop
    .eq:
        mov rax, 1
        ret
    .neq:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rsi, rsp 
    mov rdx, 1
    xor rax, rax ; input call
    xor rdi, rdi ; stdin
    syscall
    test rax, rax
    jz .end ; if rax = 0 return 0
    mov al, [rsp]
    .end:
        inc rsp ; restore rsp
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12 ; using callee-saved registers since there will be a lot of calls
    push r13
    push r14
    mov r12, rdi ; buffer begining 
    mov r13, rsi ; buffer size
    xor r14, r14 ; loop counter
    .loop:
        call read_char
        test rax, rax ; check for terminator
        jz .form_answer
        cmp r14, r13 ; check for overflow
        jnl .overflow 
        cmp rax, 0xA ; check for "\n"
        jz .space
        cmp rax, 0x9 ; check for tabulation
        jz .space
        cmp rax, 0x20 ; check for space
        jz .space
        mov [r12 + r14], al ; write symbol in memory
        inc r14
        jmp .loop
    .form_answer:
        mov byte[r14 + r12], 0 ; writing terminator
        mov rax, r12
        mov rdx, r14
        jmp .end
    .space:
        test r14, r14 ;checking if this space in the begining
        jz .loop
        jmp .form_answer
    .overflow:
        xor rax, rax
    .end:
        pop r14 ; restore r12-r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r8, r8 ; length
    xor r9, r9 
    .read_digit:
        mov r9b, byte [rdi + r8]
        test r9b, r9b; checking number
        je .end
        cmp r9b, '0'
        jl .err
        cmp r9b, '9'
        jg .err
    .parse: ; forming number
        inc r8
        sub r9b, '0'
        mul r10
        add rax, r9
        jmp .read_digit
    .err:
        cmp rax, 0
        jnz .end
        xor rdx, rdx
    .end:
        mov rdx, r8
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
	test rdx, rdx
	jz .end
    inc rdx
    neg rax
.end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax ; length
    .loop:
        cmp rax, rdx
        jge .err
        mov r8b, byte [rdi+rax]
        mov byte [rsi + rax], r8b
        test r8b, r8b
        jz .end
        inc rax
        jmp .loop
    .err:
        xor rax, rax
    .end:
        ret