%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define BUFF_SIZE 256

section .rodata
err_long_key: db "Key is too long!", 0
err_not_found: db "No such key found!", 0

section .bss
buffer: resb BUFF_SIZE ; not initialized data buffer

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUFF_SIZE - 1 ;read_word automaticly adds null-terminator so max str lengrh is BUFF_SIZE - 1
    call read_word
    test rax, rax
    jz .too_long_err ; read_word returns 0 in rax if word is too long
    push rdx ; save word length
    mov rdi, rax ; preparing inputs for find_word
    mov rsi, head
    call find_word
    pop rdx
    test rax, rax
    jz .not_found_err
    add rax, 8 ; rax now points to key
    add rax, rdx ; pointer + string length
    inc rax ; skip terminator
    mov rdi, rax
    call print_string
    .complete:
        xor rdi, rdi
        call exit
    .too_long_err:
        mov rdi, err_long_key
        jmp .error
    .not_found_err
        mov rdi, err_not_found
    .error:
        call print_error
        jmp .complete

