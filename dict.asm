%include "lib.inc"
global find_word

section .text


; Принимает два аргумента:
; rdi - указатель на нуль-терминированную строку.
; rsi - указатель на начало словаря.
; Проходит по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, возвращает адрес начала вхождения в словарь, иначе 0
find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    .loop:
        mov rdi, r12
        mov rsi, r13
        add rsi, 8 ;adding word length
        call string_equals
        test rax, rax ;checking result of equality-check
        jnz .succes ;if not zero - go to return part of func
        mov r13, [r13] ;next element
        test r13, r13 
        jnz  .loop ;if not at the end of dictionary - next iteration
    .failure:
        xor rax, rax
        jmp .exit
    .succes:
        mov rax, r13
    .exit:
        pop r13
        pop r12
        ret

